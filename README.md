**This API will serve Account and Contact Operations**

## Refer API Spec under `api` folder
## ERD Diagram under `diagram` folder

## Steps to Run this Application (locally)

Please make sure Java, Maven environment are setup and if possible docker, then Clone this repo and follow below steps

1. Building application/Docker Image - 
	cd to marketplace/ and mvn clean package
2. Run the application - 
	cd to target/ and run `java -jar marketplace-0.0.1-SNAPSHOT.jar`
	 or 
	docker run marketplace:0.0.1-SNAPSHOT
3. Verify endpoints - 
	ex: http://localhost:8080/account/1
	
Notes : Application does not connect to database, it will return success response

## Unfinished Items:
	1. API Authorization - Possibly an oAuth module to manage Authorization
	2. Database connection - JDBC Template
	3. DB statements/Queries to add/update/fetch data
	4. Build API responses - Pagination if number of contacts for account are more than 1000
	5. Tests to be executed - mockito framework
	6. Logging
	7. prepare cloudformation


## Infrastructure

Application will be deployed in AWS, and below infrastructure will be used

1. ECS - containerize the app and adding autoscaling
2. RDS - Database for Account and Contact data
3. ECR - Container Image Repository
4. ALB - load balancing
5. Cloudformation - To create infrastructure