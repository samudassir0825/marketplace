package com.atlassian.marketplace.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atlassian.marketplace.entity.Contact;

@RestController
public class ContactService {
	
	@PostMapping(path = "/contact", consumes = MediaType.APPLICATION_JSON_VALUE,  produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> createContact(@RequestBody Contact cont) {
        return new ResponseEntity<>("{\"status\": \"200 OK\"}", HttpStatus.OK);
    }
	
	@PutMapping(path = "/contact", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> updateContact(@RequestBody Contact cont) {
        return new ResponseEntity<>("{\"status\": \"200 OK\"}", HttpStatus.OK);
    }
	
    @GetMapping(path = "/contact/{contactId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> getContactInfo(@PathVariable("contactId") String contactId) {
        return new ResponseEntity<>("{\"status\": \"200 OK\"}", HttpStatus.OK);
    }
}
