package com.atlassian.marketplace.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.atlassian.marketplace.entity.Account;

@RestController
public class AccountService {
	
	@PostMapping(value = "/account", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> createAccount(@RequestBody Account acc) {
        return new ResponseEntity<>("{\"status\": \"200 OK\"}", HttpStatus.OK);
    }
	
	@PutMapping(path = "/account", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> updateAccount(@RequestBody Account acc) {
        return new ResponseEntity<>("{\"status\": \"200 OK\"}", HttpStatus.OK);
    }
	
	@GetMapping(path = "/account/{accountId}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> getAccountInfo(@PathVariable("accountId") String accountId) {
        return new ResponseEntity<>("{\"status\": \"200 OK\"}", HttpStatus.OK);
    }
    
	@GetMapping(path = "/account/{accountId}/contacts", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<String> getAccountContacts(@PathVariable("accountId") String accountId) {
        return new ResponseEntity<>("{\"status\": \"200 OK\"}", HttpStatus.OK);
    }
   
}
